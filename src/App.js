import logo from './logo.svg';
import './App.css';



//when compiling multiple component shoul be wrap in a single element
// PUBLIC folder -> the pupose is to store all documents and modules that shared accross all components of the app that are visible to all.
import luffy from './luffy.jpeg';
  //pass down the alias that identifies the img
  

function App() {
  return (
    <div>
      <h1>Welcome to the course booking batch 164</h1>
      <h5>This is our project in React</h5>
      <h6>Come visit our website</h6>
      {/* this image come from src folder*/}
      <img src={luffy} alt="image not found"/>
      {/* this image came from PUBLIC */}
      <img src="/image/luffy.jpeg"/>
      <h3>This is my favorite Cartoon Character</h3>
    </div>
  );
}

export default App;
